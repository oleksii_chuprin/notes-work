<?php

use Illuminate\Database\Seeder;

class NoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notes')->insert([
        	[
            'user_id' => 1,
            'name' => 'note 1',
            'status' => 0,
            'description' => 'note description1',
            ],
                [
            'user_id' => 3,
            'name' => 'note 2',
            'status' => 0,
            'description' => 'note description2',
        	],
        	[
            'user_id' => 3,
            'name' => 'note 3',
            'status' => 0,
            'description' => 'note description3',

        	],

        ]);
    }
}
