<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Note;

class AutoDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command auto delele notes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Note::where('created_at','<', \Carbon\Carbon::now()->addDays(-30))->delete();
    }
}
