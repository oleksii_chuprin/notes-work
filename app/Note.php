<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
        'user_id', 'name', 'description','status','src_image'
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function file(){
        return $this->belongsTo(File::class);
    }
}
