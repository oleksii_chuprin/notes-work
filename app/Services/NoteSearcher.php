<?php

namespace App\Services;


use App\Note;

class NoteSearcher
{

    private $note;


    public function __construct(Note $note)
    {
        $this->note = $note;
    }


    public function search(string $param)
    {
        return $this->searchProductByName($param);
    }

    private function searchProductByName(string $param)
    {
        return $this->note->where('name','LIKE','%'.$param.'%')->get();
    }
}
