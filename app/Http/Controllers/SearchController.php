<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use App\Services\NoteSearcher;
use Illuminate\Support\Facades\Config;

class SearchController extends Controller
{
    private $note;
    
    public function __construct(Note $note) {
        $this->note = $note;
    }
    
    public function search(\App\Services\NoteSearcher $noteSearcher){
        $colors = Config::get('colors');
        $notes = $noteSearcher->search($_POST['search']);
        return view('notes', compact('notes', 'colors'));
    }
    

}
