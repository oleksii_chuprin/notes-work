<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use Illuminate\Support\Facades\Auth;
use App\File;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Validator;

class NoteController extends Controller
{
    
    private $note;
    
    public function __construct(Note $note)
    {
        $this->note = $note;
        $this->middleware('auth');
    }
    
    public function index()
    {
        $colors = Config::get('colors');
        if(count(Auth::user()->notes)){
            $notes = $this->note->where('user_id', Auth::user()->id )->get();
            return view('notes', compact('notes', 'colors'));
        }else{
            return view('notes',['colors'=>$colors]);
        }
    }
    
    public function add_notes(Request $request)
    {
        if($request->isMethod('post')){
            $rules = [
                'name'=> 'required|max:255',
                'description' => 'required',
                'status' => 'required'
            ];
            $this->validate($request, $rules);
            $path = $request->file('file')->store('uploads','public');
            $note = new Note;
            $note->name = $request->name;
            $note->description = $request->description;
            $note->status = $request->status;
            $note->color = $request->color;
            $note->user_id = Auth::user()->id;
            $note->src_image = $path;
            $note->save();
            return redirect('/');
        }

    }

    public function delete(Request $request)
    {
        $note = $this->note->where('id', $request->get('note_id_del'))->first();
        $note->delete();
        return redirect('/');
    }
    
    public function update(Request $request)
    {
        $note = $this->note->where('id', $request->get('note_id'))->first();
        $note->user_id=$request->get('user_id');
        $note->name=$request->get('name');
        $note->description=$request->get('description');
        $note->save();
        return redirect('/');
    }

    public function update_status(Request $request)
    {
        $id = $request->id_note;
        $status = $request->status;
        $note = $this->note->where('id', $id)->first();
        $note->status = $status;
        $note->save();
    }
    
    public function share()
    {
        $notes = $this->note->where('status', 1)->get();
        return view('public_notes',['notes' => $notes]);
    }
    
    
    
}
